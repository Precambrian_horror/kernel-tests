#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /kernel/networking/kselftests
#   Description: kselftests
#   Author: Hangbin Liu <haliu@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2020 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Global parameters
# DEBUG: enable debug or not, default is true
# CHECK_UNINVES: also check uninvestigated tests result, default is false
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1
#-------------------- Setup --------------------
arch=$(uname -i)
version=$(uname -r | cut -f1 -d'-')
release=$(uname -r | cut -f2 -d'-' | sed "s/\.${arch}.*//")
SKIP_CODE=4
TMPDIR=/var/tmp/$(date +"%Y%m%d%H%M%S")
TEST_ITEMS=${TEST_ITEMS:-"default"}
if [ ${DELIVERED_TESTS} ]; then
    EXEC_DIR="/usr/libexec/kselftests"
else
    EXEC_DIR="$TMPDIR/selftests"
fi
# List of selftests to skip.
SKIP_TARGETS=${SKIP_TARGETS:-""}
INCLUDE=${INCLUDE:-""}

. ./include.sh
for file in $INCLUDE; do
    echo "Loading "$file"."
    . ./$file
done

name="kernel"
if [ -x /usr/sbin/kernel-is-rt ]; then
        name="kernel-rt"
fi

debug_dash=""
debug_dot=""
if uname -r | grep -q '+debug$'; then
    debug_dash="-debug"
    debug_dot=".debug"
fi

mkdir $TMPDIR
mkdir $EXEC_DIR
install_packages()
{
    pushd $TMPDIR
    # for 32 bit support
    if [ "${arch}" == "x86_64" ]; then
        dnf install -y glibc-devel.*i686
    fi
    if [ "$UPSTREAM_SOURCE_URL" ]; then
        wget --no-check-certificate $UPSTREAM_SOURCE_URL -O kselftest.tar.gz || test_fail_exit "Fetch Pkg Failed"
        tar zxf kselftest.tar.gz
        pushd linux-kselftest-*/
    else
        pkg=${name}-${version}-${release}
        rlFetchSrcForInstalled $pkg || test_fail_exit "Fetch Src Failed"
        rpm -ivh --define "_topdir $TMPDIR" ${name}-${version}-${release}.src.rpm
        pushd SPECS
        rlRun "yum-builddep -y ./kernel.spec"
        pushd ../SOURCES
        tar Jxf linux-${version}-${release}.tar.xz
        pushd linux-${version}-${release}/
        extraversion="-${release}.${arch}${debug_dot}"
        sed -i "s/^EXTRAVERSION =.*/EXTRAVERSION = ${extraversion}/" Makefile
    fi
    # to get Module.symvers
    rlRun "dnf install -y ${name}${debug_dash}-devel-${version}-${release}"
    symvers=$(rpm -ql "${name}${debug_dash}-devel" | grep '\<Module.symvers\>$')
    rlRun "ln -s "${symvers}" Module.symvers"
    popd
}

install_kselftests()
{
    # Install the selftests-internal, modules-internal packages by default
    if [ "${CKI_SELFTESTS_URL}" ] ; then
        pushd ${EXEC_DIR}
        wget --no-check-certificate $CKI_SELFTESTS_URL -O kselftest.tar.gz
        tar zxf kselftest.tar.gz
        rlLog "Upstream ${TEST} installed..."
        popd
    elif [ "${BUILD_FROM_SRC}" ] ; then
        if [ "${UPSTREAM_SOURCE_URL}" ]; then
            pushd $TMPDIR/linux-kselftest-*/
        else
            pushd $TMPDIR/SOURCES/linux-${version}-${release}/
        fi
        yes "" | make config
        # for bpf build
        make -j`nproc` modules_prepare
        sed -i "s/^SKIP_TARGETS.*/#SKIP_TARGETS ?= /" tools/testing/selftests/Makefile
        # issue with builddep so adding this temporarily till resolved.
        rlRun "dnf install -y rsync libcap-devel clang llvm python3-docutils numactl-devel"
        make -j`nproc` -C tools/testing/selftests install TARGETS="${TEST_ITEMS}" INSTALL_PATH=${EXEC_DIR}
        rlLog "Compiled ${TEST} installed..."
        [ -f $TMPDIR/selftests/run_kselftest.sh ] && return 0 || return 1
        popd
    else
        rlRpmInstall ${name}${debug_dash}-modules-internal ${version} ${release} ${arch}
        rlRpmInstall ${name}-selftests-internal ${version} ${release} ${arch}
        rlLog "Delivered ${TEST} installed..."
    fi
}

function NormalizeTestItems()
{
    item=$1
    total_tests=""
    grep -qE "^${item}(:|$)" ${EXEC_DIR}/kselftest-list.txt || \
        { test_skip "$item test not found in kselftest-list.txt"; }
    #add echo because += does not add white space to the end or begining of lists it processes.
    total_tests+=`echo " " $(grep -E "^${item}(:|$)" ${EXEC_DIR}/kselftest-list.txt)`
    TARGETS=${total_tests}
}

function RunKSelfTest()
{
    declare testscript="$1"
    declare log="`echo ${testscript}|tr \/ \_`.log"
    declare dmesglog="dmesg_`echo ${testscript}|tr \/ \_`.log"
    local ret

    OUTPUTFILE=$LOG_DIR/$log
    # check if the test is to be ignored
    if [[ "$SKIP_TARGETS" = *"$testscript"* ]]; then
        rlLog "=== Skipping: $testscript"
        ret=$SKIP_CODE
        return $ret
    fi
    # clear dmesg
    dmesg -c >/dev/null

    # run the self-test script
    rlLog "=== Running: $testscript"
    pushd $EXEC_DIR/`echo ${testscript}|cut -d : -f 1`
    ./`echo ${testscript}|cut -d : -f 2`|& tee $OUTPUTFILE
    ret=${PIPESTATUS[0]}
    # report the result with a copy of the dmesg log
    dmesg > $LOG_DIR/$dmesglog
    submit_log $LOG_DIR/$dmesglog
    submit_log $OUTPUTFILE
    popd
    return $ret
}

function SetupTest ()
{
    rlPhaseStartSetup
    if [ "${BUILD_FROM_SRC}" ]; then
        rlRun install_packages
        # do patches
        for item in $TEST_ITEMS; do
            _item=`echo $item | tr \/ \_`
            if type do_${_item}_patch >& /dev/null; then
                rlRun do_${_item}_patch
            fi
        done
    fi
    rlRun install_kselftests || test_fail_exit "install kselftests failed"
    submit_log "$EXEC_DIR/kselftest-list.txt"
    rlPhaseEnd
}

function RunTest ()
{
    local ret
    for item in $TEST_ITEMS; do
        rlPhaseStartTest $item
        rlLog "Test Start Time: $(date)"
        # do setup
        _item=`echo $item | tr \/ \_`
        if type do_${_item}_config >& /dev/null; then
            rlRun do_${_item}_config
        fi
        # create list of tests to run
        if [ "${TEST_ITEMS}" == "default" ]; then
            TARGETS=$(${EXEC_DIR}/run_kselftest.sh -l)
        else
            NormalizeTestItems $item
        fi
        total_num=$(echo ${TARGETS} | wc -w)
        num=0
        # Run self-tests
        for t in ${TARGETS}; do
            num=$(($num + 1))
            RunKSelfTest ${t}
            ret=$?
            check_result $num $total_num ${item} ${t} $ret
        done
        # do reset
        if type do_${_item}_reset >& /dev/null; then
            rlRun do_${_item}_reset
        fi
        rlLog "Test End Time: $(date)"
        rlPhaseEnd
    done
}

function CleanupTest ()
{
    rlPhaseStartCleanup
    rlRun "pushd '$HOME'"

    rlRun "rm -rf $TMPDIR"

    rlPhaseEnd
}

rlJournalStart

SetupTest
RunTest
CleanupTest

rlJournalEnd
